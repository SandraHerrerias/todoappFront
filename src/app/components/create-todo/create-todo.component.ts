import { TodoService } from './../../services/todo.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Todo } from 'src/app/models/todo';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.css']
})
export class CreateTodoComponent implements OnInit {

  public newTodoForm: FormGroup;
  resultado!: string;
  todo!: any;
 
  constructor(
    private todoService: TodoService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { 

    this.newTodoForm = this.formBuilder.group({
      userId: new FormControl('', [Validators.required, Validators.min(1)]),
      title: new FormControl('', [Validators.required, Validators.maxLength(199)]),
      completed: new FormControl('', [Validators.required])
    });
  }

  ngOnInit(): void {
  }

  /**
   * Get form data
   */
   get todoFormControl() {
    return this.newTodoForm.controls;
  }

  submit() {
    if (this.newTodoForm.valid)
      this.addVisit();
    else
      this.resultado = "Hay datos inválidos en el formulario";
  }

  addVisit() {
    this.todo = new Todo();
    this.todo.user = new User();
    this.todo.user.id = this.newTodoForm.value.userId;
    this.todo.title = this.newTodoForm.value.title;
    this.todo.completed = this.newTodoForm.value.completed;
 

    if (this.newTodoForm.valid) {
      this.todoService.createTodo(this.todo).subscribe(
        (result: any) => {
          if (result.success) { //success message
            alert("Todo insertado correctamente");
            this.newTodoForm.reset();
            this.redirectTodoList();
          }
        }
      );
    }
  }

  redirectTodoList() {
    this.router.navigate(['/todoslist']);
  }

}