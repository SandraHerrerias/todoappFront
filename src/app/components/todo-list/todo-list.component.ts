import { TodoService } from './../../services/todo.service';
import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/models/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  todos!: Todo[];
  filteredTodos!: Todo[];
  ipp: number;
  cp: number;
  usernameFilter: string = "";
  constructor(

    private todoService: TodoService
  ) {
    this.ipp = 5;
    this.cp = 1;
  }

  ngOnInit(): void {
    this.getTodos();
    this.todos = this.todoService.todosArray;
    this.filteredTodos = this.todos;
  }

  private getTodos() {
    this.todoService.findAllTodos().subscribe(
      userData => { this.todos = userData }
    );
  }

  filter() {
    this.filteredTodos = this.todos.filter(todo => {
      if (todo.user.username.indexOf(this.usernameFilter) != -1) {
        return true;
      }
      return false;
    });
  }
}
