export class Address {

    private _id!: number;
	private _street!: String;
	private _city!: String;
	private _zipcode!: String;
	private _country!: String;

    constructor(id?: number,
        street?: String,
        city?: String,
        zipcode?: String,
        country?: String){

            if (id != undefined) {
                this._id = id;
            }
            if (street != undefined) {
                this._street = street;
            }
            if (city != undefined) {
                this._city = city;
            }
            if (zipcode != undefined) {
                this._zipcode = zipcode;
            }
            if (country != undefined) {
                this._country = country;
            }
    }

    public get id(): number {
        return this._id;
    }

    public set id(value: number) {
        this._id = value;
    }
    public get street(): String {
        return this._street;
    }
    public set street(value: String) {
        this._street = value;
    }
    public get city(): String {
        return this._city;
    }
    public set city(value: String) {
        this._city = value;
    }

    public get zipcode(): String {
        return this._zipcode;
    }
    public set zipcode(value: String) {
        this._zipcode = value;
    }

    public get country(): String {
        return this._country;
    }
    public set country(value: String) {
        this._country = value;
    }
}
