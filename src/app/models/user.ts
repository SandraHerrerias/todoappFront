import { Address } from "./address";

export class User {

    private _id!: number;
	private _name!: String;
	private  _username!: String;
	private  _password!: String;
	private _address!: Address;

    constructor(id?: number,
        name?: String,
        username?: String,
        password?: String,
        address?: Address){

            if (id != undefined) {
                this._id = id;
            }
            if (name != undefined) {
                this._name = name;
            }
            if (username != undefined) {
                this._username = username;
            }
            if (password != undefined) {
                this._password = password;
            }
            if (address != undefined) {
                this._address = address;
            }
    }

    public get id(): number {
        return this._id;
    }

    public set id(value: number) {
        this._id = value;
    }
    public get name(): String {
        return this._name;
    }
    public set name(value: String) {
        this._name = value;
    }
    public get username(): String {
        return this._username;
    }
    public set username(value: String) {
        this._username = value;
    }

    public get password(): String {
        return this._password;
    }
    public set password(value: String) {
        this._password = value;
    }

    public get address(): Address {
        return this._address;
    }
    public set address(value: Address) {
        this._address = value;
    }
}
