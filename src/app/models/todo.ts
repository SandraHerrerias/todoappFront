import { User } from "./user";


export class Todo {
  
	private _id!: number;
	private _title!: String;
	private _completed!: boolean;
	private _user!: User;

	constructor(id?: number,
        title?: String,
        completed?: boolean,
        user?: User){

            if (id != undefined) {
                this._id = id;
            }
            if (title != undefined) {
                this._title = title;
            }
            if (completed != undefined) {
                this._completed = completed;
            }
            if (user != undefined) {
                this._user = user;
            }
    }

    public get id(): number {
        return this._id;
    }

    public set id(value: number) {
        this._id = value;
    }
    public get title(): String {
        return this._title;
    }
    public set title(value: String) {
        this._title = value;
    }
    public get completed(): boolean {
        return this._completed;
    }
    public set completed(value: boolean) {
        this._completed = value;
    }

    public get user(): User {
        return this._user;
    }
    public set user(value: User) {
        this._user = value;
    }
}
