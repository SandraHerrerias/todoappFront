import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from '../models/todo';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  todosArray: Todo[] = [];

  private backendURL: string = "http://localhost:8080/todo";
  constructor(
     private httpClient: HttpClient
  ) { }

  findAllTodos(): Observable<Todo[]>{
    return this.httpClient.get<Todo[]>(`${this.backendURL}`);
  }

  createTodo(todo: any): Observable<Object>{
    return this.httpClient.post(`${this.backendURL}`, 
    { title: todo.title,
      completed: todo.completed,
      user: {id: todo.user.id}
    },
    {
      responseType: "json"
    }
    );
  }

  updateTodo(id: number, todo: Todo): Observable<Object>{
    return this.httpClient.put(`${this.backendURL}/${id}`, todo);
  }

  deleteTodo(id: number): Observable<Object>{
    return this.httpClient.delete(`${this.backendURL}/${id}`);
  }

}
